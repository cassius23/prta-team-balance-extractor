# Team balance data extractor

## Installation

Manual setup only, packages for a key dependency are not available:

```
./venv/bin/pip install -e git+https://github.com/emilekm/pr-demo-analyzer.git
./venv/bin/python setup.py develop
```

## Running the script

```bash
$ python -m team_balance <tracker_file_path> [-O <output_file>]
```

If no output file is provided then the output is printed to `stdout`.
