import datetime
import operator
from functools import reduce
from pathlib import Path

from demo_analyzer import helpers
from demo_analyzer.constants import MessageTypes
from demo_analyzer.messages import type_to_serializer

# TODO: remove briefing time from usage/round_length/mean_player_count metrics


def handler(tracker_filename: Path):
    tracker_view = helpers.open_demo(str(tracker_filename))

    for interface in ALL_INTERFACES:
        interface.round_start()

    for message in filter(lambda msg: msg.type in USED_MESSAGE_TYPES, tracker_view):
        if message.type == MessageTypes.ROUNDEND:
            break

        serializer = type_to_serializer(message.type)
        msg = serializer(buffer=message)
        for interface in ALL_INTERFACES:
            interface.on_message(msg, message.type)

    for interface in ALL_INTERFACES:
        interface.round_end()

    total_usage = sum(player.usage() for player in Players.PLAYERS.values())
    return {
        "round": {
            "round_length": RoundMetadata.round_length,
            "total_usage": total_usage,
            "mean_player_count": total_usage / RoundMetadata.round_length,
            "start_time": RoundMetadata.start_time.strftime("%FT%T"),
            "map": RoundMetadata.map_name,
        },
        'teams': {
            1: {
                'start_tickets': Tickets.start_tickets[0],
                'end_tickets': Tickets.team1,
            },
            2: {
                'start_tickets': Tickets.start_tickets[1],
                'end_tickets': Tickets.team2,
            }
        },
        'players': [
            {
                'id': hash,
                'teams': {
                    1: {
                        'time': player.times[1]
                    },
                    2: {
                        'time': player.times[2]
                    }
                }
            }
            for hash, player in Players.PLAYERS.items()
        ],
    }


class EventInterface:
    MESSAGE_TYPES = []

    routes = {}

    @classmethod
    def round_start(cls):
        pass

    @classmethod
    def on_message(cls, message):
        pass

    @classmethod
    def round_end(cls):
        pass


class Time(EventInterface):
    MESSAGE_TYPES = [
        MessageTypes.TICKS,
    ]

    TICK_TIME_UNIT = 0.04

    start = None
    current_tick = 0

    @classmethod
    def round_start(cls):
        cls.start = None
        cls.current_tick = 0

    @classmethod
    def on_message(cls, message, msg_type):
        if msg_type == MessageTypes.TICKS:
            cls.current_tick += message.data['ticks']

        if msg_type == MessageTypes.SERVER_DETAILS:
            cls.start = message.data['start_time']

    @classmethod
    def current_time(cls):
        return cls.current_tick * cls.TICK_TIME_UNIT

    @classmethod
    def current_datetime(cls):
        return cls.start + datetime.timedelta(seconds=cls.current_time())


class Players(EventInterface):
    MESSAGE_TYPES = [
        MessageTypes.PLAYER_ADD,
        MessageTypes.PLAYER_UPDATE,
        MessageTypes.PLAYER_REMOVE,
    ]

    ACTIVE = {}
    PLAYERS = {}

    class Player:
        def __init__(self, p_id):
            self.id = p_id
            self.last_team_change = Time.current_time()
            self.current_team = 0
            self.times = {
                0: 0.0,
                1: 0.0,
                2: 0.0,
            }

        def update(self, data):
            if 'team' not in data:
                return

            new_team = data['team']

            if self.current_team == new_team:
                return

            current_time = Time.current_time()

            self.times[self.current_team] += (
                current_time - self.last_team_change
            )

            self.last_team_change = current_time
            self.current_team = new_team

        def disconnect(self):
            current_time = Time.current_time()

            self.times[self.current_team] += (
                current_time - self.last_team_change
            )

            self.last_team_change = current_time
            self.current_team = 0

        def usage(self) -> float:
            return self.times[1] + self.times[2]

    @classmethod
    def round_start(cls):
        cls.PLAYERS = {}
        cls.ACTIVE = {}

    @classmethod
    def round_end(cls):
        for player in cls.PLAYERS.values():
            player.disconnect()

    @classmethod
    def on_message(cls, message, msg_type):
        if msg_type == MessageTypes.PLAYER_ADD:
            for player_data in message.data:
                cls.connected(player_data)

        if msg_type == MessageTypes.PLAYER_UPDATE:
            for player_data in message.data:
                cls.ACTIVE[player_data['id']].update(player_data)

        if msg_type == MessageTypes.PLAYER_REMOVE:
            cls.disconnect(message.data)

    @classmethod
    def connected(cls, player_data):
        hash = player_data['hash']
        player = cls.PLAYERS.get(hash, None)
        player_id = player_data['id']
        if not player:
            player = cls.Player(player_data['hash'])
            cls.PLAYERS[hash] = player
        cls.ACTIVE[player_id] = player

    @classmethod
    def disconnect(cls, player_data):
        try:
            player = cls.ACTIVE.pop(player_data['id'])
        except KeyError:
            return
        player.disconnect()


class Tickets(EventInterface):
    MESSAGE_TYPES = [
        MessageTypes.SERVER_DETAILS,
        MessageTypes.TICKETS_TEAM1,
        MessageTypes.TICKETS_TEAM2,
    ]

    start_tickets = None
    team1 = None
    team2 = None

    @classmethod
    def on_message(cls, message, msg_type):
        if msg_type == MessageTypes.SERVER_DETAILS:
            cls.start_tickets = (message.data['tickets1'], message.data['tickets2'])

        if cls.team1 == 0 or cls.team2 == 0: return

        if msg_type == MessageTypes.TICKETS_TEAM1:
            tickets = cls.fix_tickets_underflow(message.data['tickets'], cls.start_tickets[0])
            cls.team1 = tickets

        if msg_type == MessageTypes.TICKETS_TEAM2:
            tickets = cls.fix_tickets_underflow(message.data['tickets'], cls.start_tickets[1])
            cls.team2 = tickets

    @staticmethod
    def fix_tickets_underflow(tickets, start_tickets):
        if tickets > start_tickets:
            return 0
        return tickets

    @classmethod
    def round_end(cls):
        pass


class RoundMetadata(EventInterface):
    MESSAGE_TYPES = [
        MessageTypes.SERVER_DETAILS,
    ]

    start_time = None
    round_length = None
    map_name = None

    @classmethod
    def on_message(cls, message, msg_type):
        if msg_type == MessageTypes.SERVER_DETAILS:
            cls.map_name = message.data['map']
            cls.start_time = message.data['start_time']

    @classmethod
    def round_end(cls):
        cls.round_length = Time.current_time()


ALL_INTERFACES = EventInterface.__subclasses__()
USED_MESSAGE_TYPES = set(
    reduce(operator.concat, map(lambda x: x.MESSAGE_TYPES, ALL_INTERFACES))
)
USED_MESSAGE_TYPES.add(MessageTypes.ROUNDEND)
