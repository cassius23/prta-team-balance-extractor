import argparse
import json
from pathlib import Path

from team_balance.demo_handler import handler


def main():
    parser = argparse.ArgumentParser(description='Team balance data extractor')
    parser.add_argument(
        'input',
        type=Path,
        help='Input file path',
    )
    parser.add_argument(
        "-O"
        '--output',
        dest="output",
        type=Path,
        help='Output file path',
        default=None,
        required=False,
    )
    options = parser.parse_args()

    output_data = handler(options.input)

    if options.output is None:
        print(json.dumps(output_data))
    else:
        with options.output.open('w') as output_file:
            json.dump(output_data, output_file)


if __name__ == "__main__":
    main()
